<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Undang extends Model
{
    protected $table = 'undangan';
    protected $fillable = ['email','nama','tgl_lahir','kelamin','favorit','noregis'];
}
