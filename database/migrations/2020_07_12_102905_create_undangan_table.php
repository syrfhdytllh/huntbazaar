<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUndanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('undangan', function (Blueprint $table) {
            $table->id();
            $table->string('email',255)->unique();
            $table->string('nama',255)->nullable();
            $table->string('tgl_lahir',255)->nullable();
            $table->string('kelamin',255)->nullable();
            $table->string('favorit',255)->nullable();
            $table->string('noregis',255)->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('undangan');
    }
}
