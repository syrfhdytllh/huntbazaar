@extends('layout/main')

@section('title', 'Email')

@section('container')
    <div class="container">
    <div class="row">
    <div class="col-10">
    <h1 class="mt-3">Masukkan Email Undangan</h1>
    <form method="POST" action="">
    @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Alamat Email</label>
    <input type="email" class="form-control" id="email"placeholder="Masukkan Email" name="email">
    <small id="emailHelp" class="form-text text-muted">Ini akan mengirimkan form pendaftaran peserta kepada penerima email.</small>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Example textarea</label>
    <textarea class="form-control" id="textarea" rows="3" name="textarea"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Kirim</button>
</form>
    </div>    
    </div>
    </div>

    
@endsection

