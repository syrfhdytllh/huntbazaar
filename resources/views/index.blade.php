@extends('layout/main')

@section('title', 'index')

@section('container')
    <div class="container">
    <div class="row">
    <div class="col-10">
    <h1 class="mt-3">Data Pendaftar</h1>
    
    <table class="table">
    <thead class="thead-dark">
    <tr>
    <th scope="col">#</th>
    <th scope="col">id</th>
    <th scope="col">Email</th>
    <th scope="col">Nama</th>
    <th scope="col">Tanggal Lahir</th>
    <th scope="col">Jenis Kelamin</th>
    <th scope="col">Desainer Favorit</th>
    <th scope="col">No Registrasi</th>
    </tr>
    </thead>
    
    <tbody>
    @foreach($undangan as $udg)
    <tr>
    <th scope="row">{{ $loop->iteration}}</th>
    <td>{{$udg->id}}</td>
    <td>{{$udg->email}}</td>
    <td>{{$udg->nama}}</td>
    <td>{{$udg->tgl_lahir}}</td>
    <td>{{$udg->kelamin}}</td>
    <td>{{$udg->favorit}}</td>
    <td>{{$udg->noregis}}</td>
    </tr>
    @endforeach
    </tbody>

    </table>

    </div>    
    </div>
    </div>
@endsection